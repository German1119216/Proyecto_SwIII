﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WcfService1
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IService1" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IService1
    {

        [OperationContract]
        string Sumar(int a, int b);

        [OperationContract]
        string Restar(int a, int b);

        [OperationContract]
        string Multiplicar(int a, int b);

        [OperationContract]
        string Dividir(double a, double b);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "MostarUsuarios", ResponseFormat = WebMessageFormat.Xml)]
        List<user> MostrarUsuarios;
    }
    }
