
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, and Azure
-- --------------------------------------------------
-- Date Created: 09/07/2015 09:18:48
-- Generated from EDMX file: c:\users\german aguirre\documents\visual studio 2012\Projects\EjercicioCalc\WcfService1\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Calculadoradb];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[PersonaConjunto]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PersonaConjunto];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'PersonaConjunto'
CREATE TABLE [dbo].[PersonaConjunto] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [nombre] nvarchar(max)  NOT NULL,
    [apellido] nvarchar(max)  NOT NULL,
    [telefono] nvarchar(max)  NOT NULL,
    [direccion] nvarchar(max)  NOT NULL,
    [user_Id] int  NOT NULL
);
GO

-- Creating table 'userConjunto'
CREATE TABLE [dbo].[userConjunto] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [password] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'PersonaConjunto'
ALTER TABLE [dbo].[PersonaConjunto]
ADD CONSTRAINT [PK_PersonaConjunto]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'userConjunto'
ALTER TABLE [dbo].[userConjunto]
ADD CONSTRAINT [PK_userConjunto]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [user_Id] in table 'PersonaConjunto'
ALTER TABLE [dbo].[PersonaConjunto]
ADD CONSTRAINT [FK_Personauser]
    FOREIGN KEY ([user_Id])
    REFERENCES [dbo].[userConjunto]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Personauser'
CREATE INDEX [IX_FK_Personauser]
ON [dbo].[PersonaConjunto]
    ([user_Id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------