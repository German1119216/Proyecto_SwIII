﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WcfService1
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Service1" en el código, en svc y en el archivo de configuración.
    // NOTE: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Service1.svc o Service1.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class Service1 : IService1
    {
        public string Sumar(int a, int b)
        {
            int sum = (a + b);
            return "El resultado es:  " + (sum).ToString();
        }

        public string Restar(int a, int b)
        {
            int sum = (a - b);
            return "El resultado es:  " + (sum).ToString();
        }

        public string Multiplicar(int a, int b)
        {
            int sum = (a * b);
            return "El resultado es:  " + (sum).ToString();
        }

        public string Dividir(int a, int b)
        {
            double sum = (a / b);
            return "El resultado es:  " + sum.ToString();
        }


        public string Dividir(double a, double b)
        {
            throw new NotImplementedException();
        }

        public List<user> MostarUsuarios()
        {
            try
            {
                return DB.userSet.toList();
            }
            catch (Exception)
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }
        }
    }
}